package com.cris.constant;

/**
 * 消息常量
 */
public class MessageConstant {
    public static final String DELETE_QY_FAIL = "删除岸桥失败";
    public static final String DELETE_QY_SUCCESS = "删除岸桥成功";
    public static final String ADD_QY_SUCCESS = "新增岸桥成功";
    public static final String ADD_QY_FAIL = "新增岸桥失败";
    public static final String EDIT_QY_FAIL = "编辑岸桥失败";
    public static final String EDIT_QY_SUCCESS = "编辑岸桥成功";
    public static final String QUERY_QY_SUCCESS = "查询岸桥成功";
    public static final String QUERY_QY_FAIL = "查询岸桥失败";
    public static final String UPLOAD_SUCCESS = "上传成功";
    public static final String ADD_ARMG_FAIL = "新增ARMG失败";
    public static final String ADD_ARMG_SUCCESS = "新增ARMG成功";
    public static final String DELETE_ARMG_FAIL = "删除ARMG失败";
    public static final String DELETE_ARMG_SUCCESS = "删除ARMG成功";
    public static final String QUERY_ARMG_SUCCESS = "查询ARMG成功";
    public static final String QUERY_ARMG_FAIL = "查询ARMG失败";
    public static final String EDIT_ARMG_FAIL = "编辑ARMG失败";
    public static final String EDIT_ARMG_SUCCESS = "编辑ARMG成功";



    public static final String ADD_AGV_FAIL = "新增AGV失败";
    public static final String ADD_AGV_SUCCESS = "新增AGV成功";
    public static final String DELETE_AGV_FAIL = "删除AGV失败";
    public static final String DELETE_AGV_SUCCESS = "删除AGV成功";
    public static final String QUERY_AGV_SUCCESS = "查询AGV成功";
    public static final String QUERY_AGV_FAIL = "查询AGV失败";
    public static final String EDIT_AGV_FAIL = "编辑AGV失败";
    public static final String EDIT_AGV_SUCCESS = "编辑AGV成功";


    public static final String ADD_BUFFER_FAIL = "新增缓存位失败";
    public static final String ADD_BUFFER_SUCCESS = "新增缓存位成功";
    public static final String DELETE_BUFFER_FAIL = "删除缓存位失败";
    public static final String DELETE_BUFFER_SUCCESS = "删除缓存位成功";
    public static final String QUERY_BUFFER_SUCCESS = "查询缓存位成功";
    public static final String QUERY_BUFFER_FAIL = "查询缓存位失败";
    public static final String EDIT_BUFFER_FAIL = "编辑缓存位失败";
    public static final String EDIT_BUFFER_SUCCESS = "编辑缓存位成功";


    public static final String ADD_MISSION_FAIL = "新增任务失败";
    public static final String ADD_MISSION_SUCCESS = "新增任务成功";
    public static final String DELETE_MISSION_FAIL = "删除任务失败";
    public static final String DELETE_MISSION_SUCCESS = "删除任务成功";
    public static final String QUERY_MISSION_SUCCESS = "查询任务成功";
    public static final String QUERY_MISSION_FAIL = "查询任务失败";
    public static final String EDIT_MISSION_FAIL = "编辑任务失败";
    public static final String EDIT_MISSION_SUCCESS = "编辑任务成功";

    public static final String GET_SYN_TIME_NUM_REPORT_SUCCESS = "获取同步作业统计数据成功";
    public static final String GET_SYN_TIME_NUM_REPORT_FAIL = "获取同步作业数据统计数据失败";

    public static final String GET_SEP_TIME_NUM_REPORT_SUCCESS = "获取分离作业统计数据成功";
    public static final String GET_SEP_TIME_NUM_REPORT_FAIL = "获取分离作业数据统计数据失败";

    public static final String GET_DOU_TIME_NUM_REPORT_SUCCESS = "获取对比统计数据成功";
    public static final String GET_DOU_TIME_NUM_REPORT_FAIL = "获取对比数据统计数据失败";

    public static final String ADD_SYNEFFICENCY_FAIL = "新增同步作业统计数据失败";
    public static final String ADD_SYNEFFICENCY_SUCCESS = "新增同步作业统计数据成功";
    public static final String DELETE_SYNEFFICENCY_FAIL = "删除同步作业统计数据失败";
    public static final String DELETE_SYNEFFICENCY_SUCCESS = "删除同步作业统计数据成功";
    public static final String QUERY_SYNEFFICENCY_SUCCESS = "查询同步作业统计数据成功";
    public static final String QUERY_SYNEFFICENCY_FAIL = "查询同步作业统计数据失败";
    public static final String EDIT_SYNEFFICENCY_FAIL = "编辑同步作业统计数据失败";
    public static final String EDIT_SYNEFFICENCY_SUCCESS = "编辑同步作业统计数据成功";

    public static final String ADD_SEPEFFICENCY_FAIL = "新增分离作业统计数据失败";
    public static final String ADD_SEPEFFICENCY_SUCCESS = "新增分离作业统计数据成功";
    public static final String DELETE_SEPEFFICENCY_FAIL = "删除分离作业统计数据失败";
    public static final String DELETE_SEPEFFICENCY_SUCCESS = "删除分离作业统计数据成功";
    public static final String QUERY_SEPEFFICENCY_SUCCESS = "查询分离作业统计数据成功";
    public static final String QUERY_SEPEFFICENCY_FAIL = "查询分离作业统计数据失败";
    public static final String EDIT_SEPEFFICENCY_FAIL = "编辑分离作业统计数据失败";
    public static final String EDIT_SEPEFFICENCY_SUCCESS = "编辑分离作业统计数据成功";

//
//    public static final String ADD_ARMG_FAIL = "新增检查组失败";
////    public static final String ADD_ARMG_SUCCESS = "新增检查组成功";
////    public static final String DELETE_ARMG_FAIL = "删除检查组失败";
////    public static final String DELETE_ARMG_SUCCESS = "删除检查组成功";
////    public static final String QUERY_ARMG_SUCCESS = "查询检查组成功";
////    public static final String QUERY_ARMG_FAIL = "查询检查组失败";
////    public static final String EDIT_ARMG_FAIL = "编辑检查组失败";
////    public static final String EDIT_ARMG_SUCCESS = "编辑检查组成功";

//    public static final String PIC_UPLOAD_SUCCESS = "图片上传成功";
//    public static final String PIC_UPLOAD_FAIL = "图片上传失败";
//    public static final String ADD_SETMEAL_FAIL = "新增套餐失败";
//    public static final String ADD_SETMEAL_SUCCESS = "新增套餐成功";
//    public static final String IMPORT_ORDERSETTING_FAIL = "批量导入预约设置数据失败";
//    public static final String IMPORT_ORDERSETTING_SUCCESS = "批量导入预约设置数据成功";
//    public static final String GET_ORDERSETTING_SUCCESS = "获取预约设置数据成功";
//    public static final String GET_ORDERSETTING_FAIL = "获取预约设置数据失败";
//    public static final String ORDERSETTING_SUCCESS = "预约设置成功";
//    public static final String ORDERSETTING_FAIL = "预约设置失败";
//    public static final String ADD_MEMBER_FAIL = "新增会员失败";
//    public static final String ADD_MEMBER_SUCCESS = "新增会员成功";
//    public static final String DELETE_MEMBER_FAIL = "删除会员失败";
//    public static final String DELETE_MEMBER_SUCCESS = "删除会员成功";
//    public static final String EDIT_MEMBER_FAIL = "编辑会员失败";
//    public static final String EDIT_MEMBER_SUCCESS = "编辑会员成功";
//    public static final String TELEPHONE_VALIDATECODE_NOTNULL = "手机号和验证码都不能为空";
//    public static final String LOGIN_SUCCESS = "登录成功";
//    public static final String VALIDATECODE_ERROR = "验证码输入错误";
//    public static final String QUERY_ORDER_SUCCESS = "查询预约信息成功";
//    public static final String QUERY_ORDER_FAIL = "查询预约信息失败";
//    public static final String QUERY_SETMEALLIST_SUCCESS = "查询套餐列表数据成功";
//    public static final String QUERY_SETMEALLIST_FAIL = "查询套餐列表数据失败";
//    public static final String QUERY_SETMEAL_SUCCESS = "查询套餐数据成功";
//    public static final String QUERY_SETMEAL_FAIL = "查询套餐数据失败";
//    public static final String SEND_VALIDATECODE_FAIL = "验证码发送失败";
//    public static final String SEND_VALIDATECODE_SUCCESS = "验证码发送成功";
//    public static final String SELECTED_DATE_CANNOT_ORDER = "所选日期不能进行体检预约";
//    public static final String ORDER_FULL = "预约已满";
//    public static final String HAS_ORDERED = "已经完成预约，不能重复预约";
//    public static final String ORDER_SUCCESS = "预约成功";
//    public static final String GET_USERNAME_SUCCESS = "获取当前登录用户名称成功";
//    public static final String GET_USERNAME_FAIL = "获取当前登录用户名称失败";
//    public static final String GET_MENU_SUCCESS = "获取当前登录用户菜单成功";
//    public static final String GET_MENU_FAIL = "获取当前登录用户菜单失败";
//    public static final String GET_MEMBER_NUMBER_REPORT_SUCCESS = "获取会员统计数据成功";
//    public static final String GET_MEMBER_NUMBER_REPORT_FAIL = "获取会员统计数据失败";
//    public static final String GET_SETMEAL_COUNT_REPORT_SUCCESS = "获取套餐统计数据成功";
//    public static final String GET_SETMEAL_COUNT_REPORT_FAIL = "获取套餐统计数据失败";
//    public static final String GET_BUSINESS_REPORT_SUCCESS = "获取运营统计数据成功";
//    public static final String GET_BUSINESS_REPORT_FAIL = "获取运营统计数据失败";
//    public static final String GET_SETMEAL_LIST_SUCCESS = "查询套餐列表数据成功";
//    public static final String GET_SETMEAL_LIST_FAIL = "查询套餐列表数据失败";
}
