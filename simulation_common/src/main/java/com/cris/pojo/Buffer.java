package com.cris.pojo;

import java.io.Serializable;

/**
 * @author: CrisRain
 * @title: Buffer
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:52
 * @edition: 1.0
 */
public class Buffer implements Serializable {
    private Integer id;
    private AGV agv_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AGV getAgv_id() {
        return agv_id;
    }

    public void setAgv_id(AGV agv_id) {
        this.agv_id = agv_id;
    }
}
