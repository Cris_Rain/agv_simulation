package com.cris.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author: CrisRain
 * @title: AGV
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:47
 * @edition: 1.0
 */
public class AGV implements Serializable {
    private Integer id;
    private double speed;
    private Integer status;
    private List<Mission> missions;

    Mission m;
    public long time = 0;
    public Mission getM() {
        return m;
    }

    public AGV(Integer id, Mission m) {
        this.id = id;
        this.m = m;
    }

    public AGV(Integer id) {
        this.id = id;
    }

    public AGV() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }

    public void setM(Mission m) {
        this.m = m;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
