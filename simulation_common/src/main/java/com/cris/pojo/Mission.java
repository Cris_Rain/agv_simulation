package com.cris.pojo;

import java.io.Serializable;

/**
 * @author: CrisRain
 * @title: Mission
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:49
 * @edition: 1.0
 */
public class Mission implements Serializable {
    private Integer id;
    private boolean type;
    private boolean status;

    int q;//设置关联岸桥

    int c;//设置关联场桥

    public Mission() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Mission(Integer id, int q, int c) {
        this.id = id;
        this.q = q;
        this.c = c;
    }

    public Mission(Integer id, boolean type, boolean status) {
        this.id = id;
        this.type = type;
        this.status = status;
    }
}
