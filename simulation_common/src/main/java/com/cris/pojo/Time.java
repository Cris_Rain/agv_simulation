package com.cris.pojo;

/**
 * @author: CrisRain
 * @title: Time
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2915:58
 * @edition: 1.0
 */
public class Time {
    private Integer id;
    private Integer max_time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMax_time() {
        return max_time;
    }

    public void setMax_time(Integer max_time) {
        this.max_time = max_time;
    }
}
