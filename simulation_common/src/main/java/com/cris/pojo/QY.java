package com.cris.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author: CrisRain
 * @title: QC
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
public class QY implements Serializable {
    private Integer id;
    private int time;
    private double speed;
    private List<Mission> missions;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }
}
