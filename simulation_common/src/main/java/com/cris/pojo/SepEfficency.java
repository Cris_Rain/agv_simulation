package com.cris.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author: CrisRain
 * @title: AGV
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:47
 * @edition: 1.0
 */
public class SepEfficency implements Serializable {

    private Integer id;
    private List<AGV> agvs;
    private List<ARMG> armgs;
    private List<Mission> missions;
    private List<QY> qys;
    private Long max_time;
    private Long agv_num;

    public SepEfficency(Integer id, Long max_time, Long agv_num) {
        this.id = id;
        this.max_time = max_time;
        this.agv_num = agv_num;
    }

    public SepEfficency(Long max_time, Long agv_num) {
        this.max_time = max_time;
        this.agv_num = agv_num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AGV> getAgvs() {
        return agvs;
    }

    public void setAgvs(List<AGV> agvs) {
        this.agvs = agvs;
    }

    public List<ARMG> getArmgs() {
        return armgs;
    }

    public void setArmgs(List<ARMG> armgs) {
        this.armgs = armgs;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }

    public List<QY> getQys() {
        return qys;
    }

    public void setQys(List<QY> qys) {
        this.qys = qys;
    }

    public Long getMax_time() {
        return max_time;
    }

    public void setMax_time(Long max_time) {
        this.max_time = max_time;
    }

    public Long getAgv_num() {
        return agv_num;
    }

    public void setAgv_num(Long agv_num) {
        this.agv_num = agv_num;
    }
}
