package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.ARMGDao;
import com.cris.dao.ARMGDao;
import com.cris.entity.PageResult;
import com.cris.pojo.ARMG;
import com.cris.pojo.ARMG;
import com.cris.service.ARMGService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = ARMGService.class)
@Transactional//用来增强service方法，增加事务管理
public class ARMGServiceImpl implements ARMGService {
    @Autowired
    private ARMGDao armgDao;

    //添加
    public void add(ARMG armg, Integer[] missionIds) {
        armgDao.add(armg);
        setARMGAndMission(armg.getId(),missionIds);
    }


    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<ARMG> page = armgDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和任务关联
        long count = armgDao.findByARMGId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前ARMG被任务占用，不能删除");
        }
        armgDao.deleteById(id);
    }


    public ARMG findById(Integer id) {
        return armgDao.findById(id);
    }

    //编辑
    public void edit(ARMG armg, Integer[] missionIds) {
        //根据任务id删除中间表数据（清理原有关联关系）
        armgDao.deleteAssociation(armg.getId());
        //向中间表(t_mission_armg)插入数据（建立任务和检查项关联关系）
        setARMGAndMission(armg.getId(),missionIds);
        //更新任务基本信息
        armgDao.edit(armg);
    }

    //根据ARMGid查找任务id
    public List<Integer> findMissionIdsByARMGId(Integer id) {
        return armgDao.findMissionIdsByARMGId(id);
    }

    @Override
    public int countARMG() {
        return armgDao.countARMG();
    }

    //设置对应关系
    public void setARMGAndMission(Integer armgId,Integer[] missionIds){
        if(missionIds != null && missionIds.length > 0){
            for (Integer missionId : missionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("armg_id",armgId);
                map.put("mission_id",missionId);
                armgDao.setARMGAndMission(map);
            }
        }
    }


}
