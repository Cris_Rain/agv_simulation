package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.MissionDao;
import com.cris.entity.PageResult;
import com.cris.pojo.Mission;
import com.cris.service.MissionService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: CrisRain
 * @title: MissionServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = MissionService.class)
@Transactional//用来增强service方法，增加事务管理
public class MissionServiceImpl implements MissionService {
    @Autowired
    private MissionDao missionDao;
    public void add(Mission mission) {
        missionDao.add(mission);
    }

    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<Mission> page = missionDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和检查组关联
        long count = missionDao.findByMissionId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前任务被占用，不能删除");
        }
        missionDao.deleteById(id);
    }

    //编辑
    public void edit(Mission mission) {
        missionDao.edit(mission);
    }

    public Mission findById(Integer id) {
        return missionDao.findById(id);
    }

    public List<Mission> findAll() {
        return missionDao.findAll();
    }

    @Override
    public int countMission() {
        return missionDao.countMission();
    }

    @Override
    public int countDLMission() {
        return missionDao.countDLMission();
    }

    @Override
    public int countULMission() {
        return missionDao.countULMission();
    }
}
