package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.SepEfficencyDao;
import com.cris.entity.PageResult;
import com.cris.pojo.SepEfficency;
import com.cris.service.SepEfficencyService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = SepEfficencyService.class)
@Transactional//用来增强service方法，增加事务管理
public class SepEfficencyServicempl implements SepEfficencyService {
    @Autowired
    private SepEfficencyDao sepEfficencyDao;

    //添加
    public void add(SepEfficency sepEfficency, Integer[] missionIds) {
        sepEfficencyDao.add(sepEfficency);
        setSepEfficencyAndMission(sepEfficency.getId(),missionIds);
    }


    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<SepEfficency> page = sepEfficencyDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和任务关联
        long count = sepEfficencyDao.findBySepEfficencyId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前SepEfficency被任务占用，不能删除");
        }
        sepEfficencyDao.deleteById(id);
    }


    public SepEfficency findById(Integer id) {
        return sepEfficencyDao.findById(id);
    }

    //编辑
    public void edit(SepEfficency sepEfficency, Integer[] missionIds) {
        //根据任务id删除中间表数据（清理原有关联关系）
        sepEfficencyDao.deleteAssociation(sepEfficency.getId());
        //向中间表(t_mission_sepEfficency)插入数据（建立任务和检查项关联关系）
        setSepEfficencyAndMission(sepEfficency.getId(),missionIds);
        //更新任务基本信息
        sepEfficencyDao.edit(sepEfficency);
    }

    //根据SepEfficencyid查找任务id
    public List<Integer> findMissionIdsBySepEfficencyId(Integer id) {
        return sepEfficencyDao.findMissionIdsBySepEfficencyId(id);
    }

    //设置对应关系
    public void setSepEfficencyAndMission(Integer sepEfficencyId,Integer[] missionIds){
        if(missionIds != null && missionIds.length > 0){
            for (Integer missionId : missionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("sepEfficency_id",sepEfficencyId);
                map.put("mission_id",missionId);
                sepEfficencyDao.setSepEfficencyAndMission(map);
            }
        }
    }

    public void addData(SepEfficency sepEfficency){
        sepEfficencyDao.addData(sepEfficency);
    }

    @Override
    public List<Long> findAGVNums() {
        return sepEfficencyDao.findAGVNums();
    }

    @Override
    public List<Long> findMaxTimes() {
        return sepEfficencyDao.findMaxTimes();
    }

    public void truncateTable(){
        sepEfficencyDao.truncateTable();
    }
}
