package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.AGVDao;
import com.cris.dao.AGVDao;
import com.cris.entity.PageResult;
import com.cris.pojo.AGV;
import com.cris.pojo.AGV;
import com.cris.service.AGVService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = AGVService.class)
@Transactional//用来增强service方法，增加事务管理
public class AGVServiceImpl implements AGVService {
    @Autowired
    private AGVDao agvDao;

    //添加
    public void add(AGV agv, Integer[] missionIds) {
        agvDao.add(agv);
        setAGVAndMission(agv.getId(),missionIds);
    }


    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<AGV> page = agvDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和任务关联
        long count = agvDao.findByAGVId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前AGV被任务占用，不能删除");
        }
        agvDao.deleteById(id);
    }


    public AGV findById(Integer id) {
        return agvDao.findById(id);
    }

    //编辑
    public void edit(AGV agv, Integer[] missionIds) {
        //根据任务id删除中间表数据（清理原有关联关系）
        agvDao.deleteAssociation(agv.getId());
        //向中间表(t_mission_agv)插入数据（建立任务和检查项关联关系）
        setAGVAndMission(agv.getId(),missionIds);
        //更新任务基本信息
        agvDao.edit(agv);
    }

    //根据AGVid查找任务id
    public List<Integer> findMissionIdsByAGVId(Integer id) {
        return agvDao.findMissionIdsByAGVId(id);
    }

    //设置对应关系
    public void setAGVAndMission(Integer agvId,Integer[] missionIds){
        if(missionIds != null && missionIds.length > 0){
            for (Integer missionId : missionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("agv_id",agvId);
                map.put("mission_id",missionId);
                agvDao.setAGVAndMission(map);
            }
        }
    }

    //计算agv数量
    public int countAGV(){
        System.out.println(agvDao.countAGV());
        return  agvDao.countAGV();
    }
}
