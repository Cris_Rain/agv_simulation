package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.SynEfficencyDao;
import com.cris.entity.PageResult;
import com.cris.pojo.SynEfficency;
import com.cris.service.SynEfficencyService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = SynEfficencyService.class)
@Transactional//用来增强service方法，增加事务管理
public class SynEfficencyServicempl implements SynEfficencyService {
    @Autowired
    private SynEfficencyDao synEfficencyDao;

    //添加
    public void add(SynEfficency synEfficency, Integer[] missionIds) {
        synEfficencyDao.add(synEfficency);
        setSynEfficencyAndMission(synEfficency.getId(),missionIds);
    }


    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<SynEfficency> page = synEfficencyDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和任务关联
        long count = synEfficencyDao.findBySynEfficencyId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前SynEfficency被任务占用，不能删除");
        }
        synEfficencyDao.deleteById(id);
    }


    public SynEfficency findById(Integer id) {
        return synEfficencyDao.findById(id);
    }

    //编辑
    public void edit(SynEfficency synEfficency, Integer[] missionIds) {
        //根据任务id删除中间表数据（清理原有关联关系）
        synEfficencyDao.deleteAssociation(synEfficency.getId());
        //向中间表(t_mission_synEfficency)插入数据（建立任务和检查项关联关系）
        setSynEfficencyAndMission(synEfficency.getId(),missionIds);
        //更新任务基本信息
        synEfficencyDao.edit(synEfficency);
    }

    //根据SynEfficencyid查找任务id
    public List<Integer> findMissionIdsBySynEfficencyId(Integer id) {
        return synEfficencyDao.findMissionIdsBySynEfficencyId(id);
    }

    //设置对应关系
    public void setSynEfficencyAndMission(Integer synEfficencyId,Integer[] missionIds){
        if(missionIds != null && missionIds.length > 0){
            for (Integer missionId : missionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("synEfficency_id",synEfficencyId);
                map.put("mission_id",missionId);
                synEfficencyDao.setSynEfficencyAndMission(map);
            }
        }
    }

    public void addData(SynEfficency synEfficency){
        synEfficencyDao.addData(synEfficency);
    }

    @Override
    public List<Long> findAGVNums() {
        return synEfficencyDao.findAGVNums();
    }

    @Override
    public List<Long> findMaxTimes() {
        return synEfficencyDao.findMaxTimes();
    }

    public void truncateTable(){
        synEfficencyDao.truncateTable();
    }
}
