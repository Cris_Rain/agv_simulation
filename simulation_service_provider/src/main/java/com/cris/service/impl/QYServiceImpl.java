package com.cris.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.cris.dao.QYDao;
import com.cris.entity.PageResult;
import com.cris.pojo.Mission;
import com.cris.pojo.QY;
import com.cris.service.QYService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: MissionServiceImpl
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:06
 * @edition: 1.0
 */
@Service(interfaceClass = QYService.class)
@Transactional//用来增强service方法，增加事务管理
public class QYServiceImpl implements QYService {
    @Autowired
    private QYDao qyDao;

    //添加
    public void add(QY qy,Integer[] missionIds) {
        qyDao.add(qy);
        setQYAndMission(qy.getId(),missionIds);
    }


    //分页查询
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<QY> page = qyDao.selectByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    //删除
    public void delete(Integer id) throws RuntimeException{
        //查询当前检查项是否和任务关联
        long count = qyDao.findByQYId(id);
        if(count > 0){
            //当前检查项被引用，不能删除
            throw new RuntimeException("当前岸桥被任务占用，不能删除");
        }
        qyDao.deleteById(id);
    }


    public QY findById(Integer id) {
        return qyDao.findById(id);
    }

    //编辑
    public void edit(QY qy, Integer[] missionIds) {
        //根据任务id删除中间表数据（清理原有关联关系）
        qyDao.deleteAssociation(qy.getId());
        //向中间表(t_mission_qy)插入数据（建立任务和检查项关联关系）
        setQYAndMission(qy.getId(),missionIds);
        //更新任务基本信息
        qyDao.edit(qy);
    }

    //根据岸桥id查找任务id
    public List<Integer> findMissionIdsByQYId(Integer id) {
        return qyDao.findMissionIdsByQYId(id);
    }


    public int countQY() {
        return qyDao.countQY();
    }

    //设置对应关系
    public void setQYAndMission(Integer qyId,Integer[] missionIds){
        if(missionIds != null && missionIds.length > 0){
            for (Integer missionId : missionIds) {
                Map<String,Integer> map = new HashMap<>();
                map.put("qy_id",qyId);
                map.put("mission_id",missionId);
                qyDao.setQYAndMission(map);
            }
        }
    }

}
