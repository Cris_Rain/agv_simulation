package com.cris.dao;

import com.cris.pojo.Mission;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * @author: CrisRain
 * @title: MissionDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface MissionDao {

    public void add(Mission mission);

    Page<Mission> selectByCondition(String queryString);

    public void deleteById(Integer id);

    long findByMissionId(Integer id);

    public void edit(Mission mission);

    public Mission findById(Integer id);

    public List<Mission> findAll();

    public int countMission();

    public int countDLMission();
    public int countULMission();
}
