package com.cris.dao;

import com.cris.pojo.SynEfficency;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface SynEfficencyDao {

    public void add(SynEfficency synEfficency);

    void setSynEfficencyAndMission(Map map);

    Page<SynEfficency> selectByCondition(String queryString);

    List<Integer> findMissionIdsBySynEfficencyId(Integer id);

    void deleteAssociation(Integer id);

    public void deleteById(Integer id);

    long findBySynEfficencyId(Integer id);

    public void edit(SynEfficency synEfficency);

    public SynEfficency findById(Integer id);

    void addData(SynEfficency synEfficency);

    List<Long> findAGVNums();

    List<Long> findMaxTimes();

    void truncateTable();
}
