package com.cris.dao;

import com.cris.pojo.AGV;
import com.cris.pojo.AGV;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface AGVDao {

    public void add(AGV agv);

    void setAGVAndMission(Map map);

    Page<AGV> selectByCondition(String queryString);

    List<Integer> findMissionIdsByAGVId(Integer id);

    void deleteAssociation(Integer id);

    public void deleteById(Integer id);

    long findByAGVId(Integer id);

    public void edit(AGV agv);

    public AGV findById(Integer id);

    public Integer countAGV();

}
