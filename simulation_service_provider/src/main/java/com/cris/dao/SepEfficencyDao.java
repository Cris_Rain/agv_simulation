package com.cris.dao;

import com.cris.pojo.SepEfficency;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface SepEfficencyDao {

    public void add(SepEfficency sepEfficency);

    void setSepEfficencyAndMission(Map map);

    Page<SepEfficency> selectByCondition(String queryString);

    List<Integer> findMissionIdsBySepEfficencyId(Integer id);

    void deleteAssociation(Integer id);

    public void deleteById(Integer id);

    long findBySepEfficencyId(Integer id);

    public void edit(SepEfficency sepEfficency);

    public SepEfficency findById(Integer id);

    void addData(SepEfficency sepEfficency);

    List<Long> findAGVNums();

    List<Long> findMaxTimes();

    void truncateTable();
}
