package com.cris.dao;

import com.cris.pojo.QY;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface QYDao {

    public void add(QY qy);

    void setQYAndMission(Map map);

    Page<QY> selectByCondition(String queryString);

    List<Integer> findMissionIdsByQYId(Integer id);

    void deleteAssociation(Integer id);

    public void deleteById(Integer id);

    long findByQYId(Integer id);

    public void edit(QY qy);

    public QY findById(Integer id);

    public Integer countQY();

}
