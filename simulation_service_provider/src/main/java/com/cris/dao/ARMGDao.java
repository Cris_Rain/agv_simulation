package com.cris.dao;

import com.cris.pojo.ARMG;
import com.cris.pojo.ARMG;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author: CrisRain
 * @title: CheckItemDao
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:10
 * @edition: 1.0
 */
public interface ARMGDao {

    public void add(ARMG armg);

    void setARMGAndMission(Map map);

    Page<ARMG> selectByCondition(String queryString);

    List<Integer> findMissionIdsByARMGId(Integer id);

    void deleteAssociation(Integer id);

    public void deleteById(Integer id);

    long findByARMGId(Integer id);

    public void edit(ARMG armg);

    public ARMG findById(Integer id);

    public int countARMG();


}
