package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.Mission;
import com.cris.service.MissionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/mission")
public class MissionController {

    @Reference
    private MissionService missionService;


    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody Mission mission) {
        try {
            missionService.add(mission);
        } catch (Exception e) {
            return new Result(false, MessageConstant.ADD_MISSION_FAIL);
        }
        return new Result(true, MessageConstant.ADD_MISSION_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = missionService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            missionService.delete(id);
        }catch (RuntimeException e){
            return new Result(false,e.getMessage());
        }catch (Exception e){
            return new Result(false, MessageConstant.DELETE_MISSION_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_MISSION_SUCCESS);
    }

    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody Mission mission){
        try {
            missionService.edit(mission);
        }catch (Exception e){
            return new Result(false,MessageConstant.EDIT_MISSION_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_MISSION_SUCCESS);
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            Mission mission = missionService.findById(id);
            return  new Result(true, MessageConstant.QUERY_MISSION_SUCCESS,mission);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }

    //查询所有
    @RequestMapping("/findAll")
    public Result findAll(){
        List<Mission> missionList = missionService.findAll();
        if(missionList != null && missionList.size() > 0){
            Result result = new Result(true, MessageConstant.QUERY_MISSION_SUCCESS);
            result.setData(missionList);
            return result;
        }
        return new Result(false,MessageConstant.QUERY_MISSION_FAIL);
    }


}
