package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.SepEfficency;
import com.cris.service.SepEfficencyService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: SepEfficency_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/SepEfficency")
public class SepEfficencyController {

    @Reference
    private SepEfficencyService sepEfficencyService;

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody SepEfficency sepEfficency, Integer[] missionIds) {
        try {
            sepEfficencyService.add(sepEfficency, missionIds);
        } catch (Exception e) {
            //新增失败
            return new Result(false, MessageConstant.ADD_SEPEFFICENCY_FAIL);
        }
        //新增成功
        return new Result(true, MessageConstant.ADD_SEPEFFICENCY_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = sepEfficencyService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            sepEfficencyService.delete(id);
        } catch (RuntimeException e) {
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            return new Result(false, MessageConstant.DELETE_SEPEFFICENCY_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_SEPEFFICENCY_SUCCESS);
    }


    //根据id查询
    @RequestMapping("/findById")
    public Result findById(Integer id) {
        try {
            SepEfficency sepEfficency = sepEfficencyService.findById(id);
            if(sepEfficency != null){
                Result result = new Result(true, MessageConstant.QUERY_SEPEFFICENCY_SUCCESS);
                result.setData(sepEfficency);
                return result;
            }
            return new Result(true, MessageConstant.QUERY_SEPEFFICENCY_SUCCESS, sepEfficency);
        } catch (Exception e) {
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_SEPEFFICENCY_FAIL);
        }
    }

    //根据SepEfficencyid查询对应的所有任务id
    @RequestMapping("/findMissionIdsBySepEfficencyId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
        try{
            List<Integer> missionIds =
                    sepEfficencyService.findMissionIdsBySepEfficencyId(id);
            return new Result(true,MessageConstant.QUERY_MISSION_SUCCESS,missionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody SepEfficency sepEfficency, Integer[] missionIds) {
        try {
            sepEfficencyService.edit(sepEfficency,missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_SEPEFFICENCY_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_SEPEFFICENCY_SUCCESS);
    }
}
