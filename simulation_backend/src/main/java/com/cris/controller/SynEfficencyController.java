package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.SynEfficency;

import com.cris.service.SynEfficencyService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: SynEfficency_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/SynEfficency")
public class SynEfficencyController {

    @Reference
    private SynEfficencyService synEfficencyService;

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody SynEfficency synEfficency, Integer[] missionIds) {
        try {
            synEfficencyService.add(synEfficency, missionIds);
        } catch (Exception e) {
            //新增失败
            return new Result(false, MessageConstant.ADD_SYNEFFICENCY_FAIL);
        }
        //新增成功
        return new Result(true, MessageConstant.ADD_SYNEFFICENCY_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = synEfficencyService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            synEfficencyService.delete(id);
        } catch (RuntimeException e) {
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            return new Result(false, MessageConstant.DELETE_SYNEFFICENCY_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_SYNEFFICENCY_SUCCESS);
    }


    //根据id查询
    @RequestMapping("/findById")
    public Result findById(Integer id) {
        try {
            SynEfficency synEfficency = synEfficencyService.findById(id);
            if(synEfficency != null){
                Result result = new Result(true, MessageConstant.QUERY_SYNEFFICENCY_SUCCESS);
                result.setData(synEfficency);
                return result;
            }
            return new Result(true, MessageConstant.QUERY_SYNEFFICENCY_SUCCESS, synEfficency);
        } catch (Exception e) {
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_SYNEFFICENCY_FAIL);
        }
    }

    //根据SynEfficencyid查询对应的所有任务id
    @RequestMapping("/findMissionIdsBySynEfficencyId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
        try{
            List<Integer> missionIds =
                    synEfficencyService.findMissionIdsBySynEfficencyId(id);
            return new Result(true,MessageConstant.QUERY_MISSION_SUCCESS,missionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody SynEfficency synEfficency, Integer[] missionIds) {
        try {
            synEfficencyService.edit(synEfficency,missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_SYNEFFICENCY_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_SYNEFFICENCY_SUCCESS);
    }
}
