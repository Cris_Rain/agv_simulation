package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.AGV;
import com.cris.pojo.AGV;
import com.cris.service.AGVService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/agv")
public class AGVController {

    @Reference
    private AGVService agvService;

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody AGV agv, Integer[] missionIds) {
        try {
            agvService.add(agv, missionIds);
        } catch (Exception e) {
            //新增失败
            return new Result(false, MessageConstant.ADD_AGV_FAIL);
        }
        //新增成功
        return new Result(true, MessageConstant.ADD_AGV_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = agvService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            agvService.delete(id);
        } catch (RuntimeException e) {
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            return new Result(false, MessageConstant.DELETE_AGV_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_AGV_SUCCESS);
    }


    //根据id查询
    @RequestMapping("/findById")
    public Result findById(Integer id) {
        try {
            AGV agv = agvService.findById(id);
            if(agv != null){
                Result result = new Result(true, MessageConstant.QUERY_AGV_SUCCESS);
                result.setData(agv);
                return result;
            }
            return new Result(true, MessageConstant.QUERY_AGV_SUCCESS, agv);
        } catch (Exception e) {
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_AGV_FAIL);
        }
    }

    //根据AGVid查询对应的所有任务id
    @RequestMapping("/findMissionIdsByAGVId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
        try{
            List<Integer> missionIds =
                    agvService.findMissionIdsByAGVId(id);
            return new Result(true,MessageConstant.QUERY_MISSION_SUCCESS,missionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody AGV agv,Integer[] missionIds) {
        try {
            agvService.edit(agv,missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_AGV_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_AGV_SUCCESS);
    }
}
