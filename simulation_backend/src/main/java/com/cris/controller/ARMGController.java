package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.ARMG;
import com.cris.pojo.ARMG;
import com.cris.service.ARMGService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/armg")
public class ARMGController {

    @Reference
    private ARMGService armgService;

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody ARMG armg, Integer[] missionIds) {
        try {
            armgService.add(armg, missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.ADD_ARMG_FAIL);
        }
        return new Result(true, MessageConstant.ADD_ARMG_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = armgService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            armgService.delete(id);
        }catch (RuntimeException e){
            return new Result(false,e.getMessage());
        }catch (Exception e){
            return new Result(false, MessageConstant.DELETE_ARMG_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_ARMG_SUCCESS);
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            ARMG armg = armgService.findById(id);
            if(armg != null){
                Result result = new Result(true, MessageConstant.QUERY_ARMG_SUCCESS);
                result.setData(armg);
                return result;
            }
            return  new Result(true, MessageConstant.QUERY_ARMG_SUCCESS,armg);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_ARMG_FAIL);
        }
    }

    //根据ARMGid查询对应的所有任务id
    @RequestMapping("/findMissionIdsByARMGId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
        try{
            List<Integer> missionIds =
                    armgService.findMissionIdsByARMGId(id);
            return new Result(true,MessageConstant.QUERY_MISSION_SUCCESS,missionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }

    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody ARMG armg,Integer[] missionIds) {
        try {
            armgService.edit(armg,missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_ARMG_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_ARMG_SUCCESS);
    }
}
