package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.PageResult;
import com.cris.entity.QueryPageBean;
import com.cris.entity.Result;
import com.cris.pojo.QY;
import com.cris.service.QYService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/qy")
public class QYController {

    @Reference
    private QYService qyService;

    //新增
    @RequestMapping("/add")
    public Result add(@RequestBody QY qy, Integer[] missionIds) {
        try {
            qyService.add(qy, missionIds);
        } catch (Exception e) {
            //新增失败
            return new Result(false, MessageConstant.ADD_QY_FAIL);
        }
        //新增成功
        return new Result(true, MessageConstant.ADD_QY_SUCCESS);
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = qyService.pageQuery(
                queryPageBean.getCurrentPage(),
                queryPageBean.getPageSize(),
                queryPageBean.getQueryString());
        return pageResult;
    }

    //删除
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            qyService.delete(id);
        } catch (RuntimeException e) {
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            return new Result(false, MessageConstant.DELETE_QY_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_QY_SUCCESS);
    }


    //根据id查询
    @RequestMapping("/findById")
    public Result findById(Integer id) {
        try {
            QY qy = qyService.findById(id);
            if(qy != null){
                Result result = new Result(true, MessageConstant.QUERY_QY_SUCCESS);
                result.setData(qy);
                return result;
            }
            return new Result(true, MessageConstant.QUERY_QY_SUCCESS, qy);
        } catch (Exception e) {
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.QUERY_QY_FAIL);
        }
    }

    //根据QYid查询对应的所有任务id
    @RequestMapping("/findMissionIdsByQYId")
    public Result findCheckItemIdsByCheckGroupId(Integer id){
        try{
            List<Integer> missionIds =
                    qyService.findMissionIdsByQYId(id);
            return new Result(true,MessageConstant.QUERY_MISSION_SUCCESS,missionIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MISSION_FAIL);
        }
    }
    //编辑
    @RequestMapping("/edit")
    public Result edit(@RequestBody QY qy,Integer[] missionIds) {
        try {
            qyService.edit(qy,missionIds);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_QY_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_QY_SUCCESS);
    }
}
