package com.cris.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cris.constant.MessageConstant;
import com.cris.entity.Result;
import com.cris.pojo.*;
import com.cris.service.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author: CrisRain
 * @title: SetQcParameterController
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2516:44
 * @edition: 1.0
 */
@RestController//包含controller与responsebody
@RequestMapping("/report")
public class ReportController {

    @Reference
    private SynEfficencyService synEfficencyService;

    @Reference
    private SepEfficencyService sepEfficencyService;

    @Reference
    private AGVService agvService;

    @Reference
    private ARMGService armgService;

    @Reference
    private QYService qyService;

    @Reference
    private MissionService missionService;
//
//    @Reference
//    private BufferService bufferService;


    /**
     * agv运行时间与数量统计 折线图数据
     * @return
     */
    @RequestMapping("/getSynReport")
    public Result getSynEfficencyReport(){
        synEfficencyService.truncateTable();

//        int AGV_NUM = agvService.countAGV();
        int id = 1;
        for(int agv_num = 100;agv_num < 2000;agv_num++){
            AGV_Dispatch_Improved2(id,agv_num);
            id++;
        }
        Map<Object,Object> map = new HashMap<>();
        List<Long> max_times = synEfficencyService.findMaxTimes();

        map.put("max_times", max_times);

        List<Long> agv_nums = synEfficencyService.findAGVNums();
        map.put("agv_nums", agv_nums);

        return new Result(true,MessageConstant. GET_SYN_TIME_NUM_REPORT_SUCCESS, map);
    }

    @RequestMapping("/getSepReport")
    public Result getSepEfficencyReport(){
        sepEfficencyService.truncateTable();

//        int AGV_NUM = agvService.countAGV();
        int id = 1;
        for(int agv_num = 100;agv_num <2000;agv_num++){
            AGV_Dispatch_Original(id,agv_num);
            id++;

        }
        Map<Object,Object> map = new HashMap<>();
        List<Long> max_times = sepEfficencyService.findMaxTimes();

        map.put("max_times", max_times);

        List<Long> agv_nums = sepEfficencyService.findAGVNums();
        map.put("agv_nums", agv_nums);

        return new Result(true,MessageConstant. GET_SEP_TIME_NUM_REPORT_SUCCESS, map);
    }

    @RequestMapping("/getSepSynReport")
    public Result getEfficencyReport(){
        sepEfficencyService.truncateTable();
        synEfficencyService.truncateTable();
        int id = 1;
        for(int agv_num = 100;agv_num <2000;agv_num++){
            AGV_Dispatch_Original(id,agv_num);
            AGV_Dispatch_Improved2(id,agv_num);
            id++;
        }
        Map<Object,Object> map = new HashMap<>();
        List<Long> max_times_sep = sepEfficencyService.findMaxTimes();
        List<Long> max_times_syn = synEfficencyService.findMaxTimes();
        map.put("max_times_sep", max_times_sep);
        map.put("max_times_syn", max_times_syn);
        List<Long> agv_nums_sep = sepEfficencyService.findAGVNums();
        List<Long> agv_nums_syn = synEfficencyService.findAGVNums();
        map.put("agv_nums_sep", agv_nums_sep);
        map.put("agv_nums_syn", agv_nums_syn);
        return new Result(true,MessageConstant. GET_DOU_TIME_NUM_REPORT_SUCCESS, map);
    }

    //同步作业
    private void AGV_Dispatch_Improved2(int id, long AGV_NUM){

        int q = qyService.countQY();//岸桥数量
        int c = armgService.countARMG();//场桥数量
        int qw = 2;//岸桥侧缓存位数量
        int cw = 2;//场桥侧缓存位数量
//        int F =  missionService.countMission();//任务总数量
        int F =  0;//任务总数量
//        int AGV_NUM = agvService.countAGV();
        long A = 0;
        int M = missionService.countULMission()*100;//卸载任务数量
        int N = missionService.countDLMission()*100;//装载任务数量
        int Z;//最大完工时间
        int f_1;//进口任务总时长
        int f_2;//出口任务总时长
        int f_time = 0;//任务总时长
        int q_t = 3 * 60 * 1000;//岸桥作业时长
        int c_t = 3 * 60 * 1000;//场桥作业时长
        int[][] t_qw_ij = new int[q][qw]; //AGV从岸桥i到缓存区j的行走时间(二维数组)
        int[][] t_cw_ij = new int[c][cw]; //AGV从场桥i到缓存区j的行走时间(二维数组)
        int c_g_n = 0;

        //正在进任务的队列（先进先出）
        ArrayList<AGV> agvs = new ArrayList<>();
        int A1 = (int) (A);
        Queue<Mission> on_mission = new ArrayDeque<>(A1);//正在进行任务等于agv数量
        Queue<Mission> in_ms = new ArrayDeque<>();//进口箱任务队列，一个一个出队列，加到正在进行任务队列当中
        Queue<Mission> out_ms = new ArrayDeque<>();//出口箱任务队列，一个一个出队列，加到正在进行任务队列当中

        /**
         * 设置运输时间
         */
        int t_temp = 40000;//初始化临时时间参数
        for (int i = 0; i < t_qw_ij.length; i++) {
            t_temp = 40000;//对各个岸桥设置到达缓存位的时间进行初始化
            for (int j = 0; j < t_qw_ij[i].length; j++) {//设置此岸桥i到缓存位j的距离
                t_temp += 5000;
                t_qw_ij[i][j] = t_temp;
            }
        }
        t_temp = 4000;//再次初始化临时时间参数
        for (int i = 0; i < t_cw_ij.length; i++) {
            t_temp = 40000;//对各个场桥设置到达缓存位的时间进行初始化
            for (int j = 0; j < t_cw_ij[i].length; j++) {//设置此场桥i到缓存位j的距离
                t_temp += 5000;
                t_cw_ij[i][j] = t_temp;
            }
        }

        //出口任务队列
        /*
        定义定量
         */
        int t_q = 3 * 60 * 1000;//从岸桥下方搬到场桥所需时间(ms)
        int t_c = 3 * 60 * 1000;//从岸桥下方搬到场桥所需时间(ms)

        A = AGV_NUM;
        //将agv，任务加载到对象中，并将agv的上一个任务统一设置为任务before_m,id为0
        Mission before_m = new Mission();
        //agv对象创建

        for (int i = 1; i <= A; i++) {
            AGV a = new AGV(i);
            a.setM(before_m);
            agvs.add(a);
//            System.out.println("AGV_id"+a.getId()+"创建成功！");
        }
        //进口任务对象创建
        for (Integer i = 1; i <= M; i++) {
//            Mission m = missionService.findById(i);
            Mission m = new Mission(i,false,false);
//            Mission m = new Mission(i,false,false);
            in_ms.add(m);
//            System.out.println("卸船任务_id:"+m.getId()+"创建成功！");
        }
        //出口任务对象创建
        for (Integer i = 1; i <= N; i++) {
            Mission n = new Mission(i,true,false);
            out_ms.add(n);
//            System.out.println("装船任务_id:"+n.getId()+"创建成功！");
        }
        F = M + N;//任务总数量为进出口任务数量之和






        /*
        表达约束
        1.一个agv只能同时运输一个集装箱
        2.Agv在场桥处完成进口任务后的下一个任务为出口。
        3.Agv在岸桥处完成进口任务后的下一个任务为出口。
        4.缓存区的存在
         */
        //当进出口任务队列都不为空时，执行循环
        while (!in_ms.isEmpty() || !out_ms.isEmpty()) {//进出口任务都为空时结束循环

            for (int i = 0; i < A; i++) {
                AGV agv = agvs.get(i);

                if (agv.getM() != null && agv.getM().isType()==false) {//如果agv有卸箱任务，则下个任务为进口箱任务
                    Mission out_m = out_ms.poll();
                    agv.setM(out_m);
                    agv.time = agv.time + t_q + q_t;
                    c_g_n+=1;
                } else if (agv.getM() != null && agv.getM().isType()==true) {//如果agv有进口箱任务，则下个任务为进口箱任务
                    Mission in_m = in_ms.poll();
                    agv.setM(in_m);
                    agv.time = agv.time + t_c + c_t;
                    c_g_n+=1;
                } else {//执行分离作业
                    if (!in_ms.isEmpty()) {
                        Mission in_m = in_ms.poll();
                        agv.setM(in_m);
                        agv.time = agv.time + t_c + t_q + q_t;
                        c_g_n+=2;
                    }
                    if (!out_ms.isEmpty()) {
                        Mission out_m = out_ms.poll();
                        agv.setM(out_m);
                        agv.time = agv.time + t_q + t_q + c_t;
                        c_g_n+=2;
                    }
                }
            }
        }
        long max_time = 0;
        for (int i = 0; i < agvs.size(); i++) {
//            System.out.println("agv" + i + "-time = " + agvs.get(i).time);
            max_time = Math.max(max_time, agvs.get(i).time);
        }
//        System.out.println("双纵作业最大时长为：" + max_time);

//        System.out.println("双纵作业来回总次数"+ c_g_n);

        SynEfficency synEfficency = new SynEfficency(id,max_time,AGV_NUM);
        synEfficencyService.addData(synEfficency);
//        return max_time;
//        List<String> list = new ArrayList<>();
//        for(int i=0;i<12;i++){
//            calendar.add(Calendar.MONTH,1);
//            list.add(new SimpleDateFormat("yyyy.MM").format(calendar.getTime()));
//        }

//        Map<String,Object> map = new HashMap<>();
//        map.put("months",list);
//
//        List<Integer> memberCount = memberService.findMemberCountByMonth(list);
//        map.put("memberCount",memberCount);
//


///**
// * 测试用例
// */
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MONTH,-12);//获得当前日期之前12个月的日期
//
//        List<String> list = new ArrayList<>();
//        for(int i=0;i<12;i++){
//            calendar.add(Calendar.MONTH,1);
//            list.add(new SimpleDateFormat("yyyy.MM").format(calendar.getTime()));
//        }
//
//        Map<String,Object> map = new HashMap<>();
//        map.put("months",list);
//
//        List<Integer> ResultCount = ResultService.findResultCountByMonth(list);
//        map.put("ResultCount",ResultCount);
//
//        return new Result(true, MessageConstant.GET_Result_NUMBER_REPORT_SUCCESS,map);
    }

    //分离作业
    private void AGV_Dispatch_Original(int id, long AGV_NUM){

        int q = qyService.countQY();//岸桥数量
        int c = armgService.countARMG();//场桥数量
        int qw = 2;//岸桥侧缓存位数量
        int cw = 2;//场桥侧缓存位数量
//        int F =  missionService.countMission();//任务总数量
        int F =  0;//任务总数量
//        int AGV_NUM = agvService.countAGV();
        long A = 0;
        int M = missionService.countULMission()*100;//卸载任务数量
        int N = missionService.countDLMission()*100;//装载任务数量
        int Z;//最大完工时间
        int f_1;//进口任务总时长
        int f_2;//出口任务总时长
        int f_time = 0;//任务总时长
        int q_t = 3 * 60 * 1000;//岸桥作业时长
        int c_t = 3 * 60 * 1000;//场桥作业时长
        int[][] t_qw_ij = new int[q][qw]; //AGV从岸桥i到缓存区j的行走时间(二维数组)
        int[][] t_cw_ij = new int[c][cw]; //AGV从场桥i到缓存区j的行走时间(二维数组)
        int c_g_n = 0;

        //正在进任务的队列（先进先出）
        ArrayList<AGV> agvs = new ArrayList<>();
        int A1 = (int) (A);
        Queue<Mission> on_mission = new ArrayDeque<>(A1);//正在进行任务等于agv数量
        Queue<Mission> in_ms = new ArrayDeque<>();//进口箱任务队列，一个一个出队列，加到正在进行任务队列当中
        Queue<Mission> out_ms = new ArrayDeque<>();//出口箱任务队列，一个一个出队列，加到正在进行任务队列当中

        /**
         * 设置运输时间
         */
        int t_temp = 40000;//初始化临时时间参数
        for (int i = 0; i < t_qw_ij.length; i++) {
            t_temp = 40000;//对各个岸桥设置到达缓存位的时间进行初始化
            for (int j = 0; j < t_qw_ij[i].length; j++) {//设置此岸桥i到缓存位j的距离
                t_temp += 5000;
                t_qw_ij[i][j] = t_temp;
            }
        }
        t_temp = 4000;//再次初始化临时时间参数
        for (int i = 0; i < t_cw_ij.length; i++) {
            t_temp = 40000;//对各个场桥设置到达缓存位的时间进行初始化
            for (int j = 0; j < t_cw_ij[i].length; j++) {//设置此场桥i到缓存位j的距离
                t_temp += 5000;
                t_cw_ij[i][j] = t_temp;
            }
        }

        //出口任务队列
        /*
        定义定量
         */
        int t_q = 3 * 60 * 1000;//从岸桥下方搬到场桥所需时间(ms)
        int t_c = 3 * 60 * 1000;//从岸桥下方搬到场桥所需时间(ms)

        A = AGV_NUM;
        //将agv，任务加载到对象中，并将agv的上一个任务统一设置为任务before_m,id为0
        Mission before_m = new Mission();
        //agv对象创建

        for (int i = 1; i <= A; i++) {
            AGV a = new AGV(i);
            a.setM(before_m);
            agvs.add(a);
//            System.out.println("AGV_id"+a.getId()+"创建成功！");
        }
        //进口任务对象创建
        for (Integer i = 1; i <= M; i++) {
//            Mission m = missionService.findById(i);
            Mission m = new Mission(i,false,false);
//            Mission m = new Mission(i,false,false);
            in_ms.add(m);
//            System.out.println("卸船任务_id:"+m.getId()+"创建成功！");
        }
        //出口任务对象创建
        for (Integer i = 1; i <= N; i++) {
            Mission n = new Mission(i,true,false);
            out_ms.add(n);
//            System.out.println("装船任务_id:"+n.getId()+"创建成功！");
        }
        F = M + N;//任务总数量为进出口任务数量之和






        /*
        表达约束
        1.一个agv只能同时运输一个集装箱
        2.Agv在场桥处完成进口任务后的下一个任务为出口。
        3.Agv在岸桥处完成进口任务后的下一个任务为出口。
        4.缓存区的存在
         */
        //当进出口任务队列都不为空时，执行循环
        while (!in_ms.isEmpty() || !out_ms.isEmpty()) {//进出口任务都为空时结束循环

            for (int i = 0; i < A; i++) {
                AGV agv = agvs.get(i);
                if (!in_ms.isEmpty()) {//进口作业，需等待岸桥装箱上车
                    Mission in_m = in_ms.poll();
                    agv.setM(in_m);
                    agv.time += t_c + t_q + q_t;
                    c_g_n+=2;
                }
                if (!out_ms.isEmpty()) {//出口作业，需等待场桥装箱上车
                    Mission out_m = out_ms.poll();
                    agv.setM(out_m);
                    agv.time += t_q + t_q + c_t;
                    c_g_n+=2;
                }
            }
        }
        long max_time = 0;
        for (int i = 0; i < agvs.size(); i++) {
//            System.out.println("agv" + i + "-time = " + agvs.get(i).time);
            max_time = Math.max(max_time, agvs.get(i).time);
        }
//        System.out.println("双纵作业最大时长为：" + max_time);

//        System.out.println("双纵作业来回总次数"+ c_g_n);

        SepEfficency sepEfficency = new SepEfficency(id,max_time,AGV_NUM);
        sepEfficencyService.addData(sepEfficency);//在表中储存数据
//        return max_time;
//        List<String> list = new ArrayList<>();
//        for(int i=0;i<12;i++){
//            calendar.add(Calendar.MONTH,1);
//            list.add(new SimpleDateFormat("yyyy.MM").format(calendar.getTime()));
//        }

//        Map<String,Object> map = new HashMap<>();
//        map.put("months",list);
//
//        List<Integer> memberCount = memberService.findMemberCountByMonth(list);
//        map.put("memberCount",memberCount);
//


///**
// * 测试用例
// */
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MONTH,-12);//获得当前日期之前12个月的日期
//
//        List<String> list = new ArrayList<>();
//        for(int i=0;i<12;i++){
//            calendar.add(Calendar.MONTH,1);
//            list.add(new SimpleDateFormat("yyyy.MM").format(calendar.getTime()));
//        }
//
//        Map<String,Object> map = new HashMap<>();
//        map.put("months",list);
//
//        List<Integer> ResultCount = ResultService.findResultCountByMonth(list);
//        map.put("ResultCount",ResultCount);
//
//        return new Result(true, MessageConstant.GET_Result_NUMBER_REPORT_SUCCESS,map);
    }


}
