package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.QY;

import java.util.List;

/**
 * @author: CrisRain
 * @title: CheckItemService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface QYService {
    public void add(QY qy,Integer[] missions);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public QY findById(Integer id);

    public void edit(QY qy,Integer[] missionIds);

    List<Integer> findMissionIdsByQYId(Integer id);

    public int countQY();
}
