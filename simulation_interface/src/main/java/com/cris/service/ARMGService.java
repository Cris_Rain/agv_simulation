package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.ARMG;
import com.cris.pojo.ARMG;

import java.util.List;

/**
 * @author: CrisRain
 * @title: CheckItemService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface ARMGService {
    public void add(ARMG armg, Integer[] missions);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public ARMG findById(Integer id);

    public void edit(ARMG armg,Integer[] missionIds);

    List<Integer> findMissionIdsByARMGId(Integer id);

    int countARMG();
}
