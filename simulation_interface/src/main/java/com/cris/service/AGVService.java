package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.AGV;
import com.cris.pojo.AGV;

import java.util.List;

/**
 * @author: CrisRain
 * @title: CheckItemService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface AGVService {
    public void add(AGV agv, Integer[] missions);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public AGV findById(Integer id);

    public void edit(AGV agv,Integer[] missionIds);

    List<Integer> findMissionIdsByAGVId(Integer id);

    public int countAGV();
}
