package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.Mission;

import java.util.List;

/**
 * @author: CrisRain
 * @title: MissionService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface MissionService {
    public void add(Mission mission);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public Mission findById(Integer id);

    public void edit(Mission mission);

    public List<Mission> findAll();

    int countMission();

    int countDLMission();
    int countULMission();
}
