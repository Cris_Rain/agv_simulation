package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.SynEfficency;

import java.util.List;

/**
 * @author: CrisRain
 * @title: CheckItemService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface SynEfficencyService {
    public void add(SynEfficency synEfficency, Integer[] missions);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public SynEfficency findById(Integer id);

    public void edit(SynEfficency synEfficency,Integer[] missionIds);

    List<Integer> findMissionIdsBySynEfficencyId(Integer id);

    public void addData(SynEfficency synEfficency);

    List<Long> findAGVNums();

    List<Long> findMaxTimes();

    void truncateTable();
}
