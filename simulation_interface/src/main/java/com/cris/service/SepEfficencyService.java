package com.cris.service;

import com.cris.entity.PageResult;
import com.cris.pojo.SepEfficency;

import java.util.List;

/**
 * @author: CrisRain
 * @title: CheckItemService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/3/2323:03
 * @edition: 1.0
 */
public interface SepEfficencyService {
    public void add(SepEfficency sepEfficency, Integer[] missions);

    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    public void delete(Integer id);

    public SepEfficency findById(Integer id);

    public void edit(SepEfficency sepEfficency,Integer[] missionIds);

    List<Integer> findMissionIdsBySepEfficencyId(Integer id);

    public void addData(SepEfficency sepEfficency);

    List<Long> findAGVNums();

    List<Long> findMaxTimes();

    void truncateTable();
}
