package com.cris.service;

/**
 * @author: CrisRain
 * @title: ReportService
 * @projectName: AGV_Simulation
 * @description: write something here......
 * @date: 2022/4/717:07
 * @edition: 1.0
 */
public interface ReportService {
    public void AGV_Dispatch_Original(int id, long AGV_NUM);
    public void AGV_Dispatch_Improved2(int id, long AGV_NUM);
}
